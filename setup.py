import datetime

from setuptools import setup

TIMESTAMP = str(datetime.datetime.now().replace(microsecond=0).isoformat()).\
    replace('-', '').replace('T', '').replace(':', '')

setup(
    name='mosaik.CSV_SemVer',
    version='1.0.4' + 'rc' + TIMESTAMP,
    author='Stefan Scherfke',
    author_email='mosaik@offis.de',
    description=('Presents CSV datasets to mosaik as models.'),
    long_description=(open('README.rst').read() + '\n\n' +
                      open('CHANGES.txt').read() + '\n\n' +
                      open('AUTHORS.txt').read()),
    url='https://bitbucket.org/mosaik/mosaik-csv',
    py_modules=['mosaik_csv'],
    install_requires=[
        'arrow>=0.9.0',
        'mosaik.API-SemVer>=2.4.2rc20201202042235',
    ],
    include_package_data=True,
    maintainer='Bengt Lüers',
    maintainer_email='bengt.lueers@gmail.com',
    entry_points={
        'console_scripts': [
            'mosaik-csv = mosaik_csv.mosaik:main',
        ],
    },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU Lesser General Public License v2 (LGPLv2)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Scientific/Engineering',
    ],
)
