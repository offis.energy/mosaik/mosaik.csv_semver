mosaik-csv
==========

This is pseudo simulator that presents CSV data sets to mosaik as models.

Prerequisites
-------------

If installation of psutil fails, installing python developer edition and gcc should help::

    $ sudo apt-get install gcc python3-dev

Tests
-----

You can run the tests with::

    $ git clone https://bitbucket.org/mosaik/mosaik-csv
    $ cd mosaik-csv
    $ python3.8 -m venv venv
    $ venv/bin/python -m pip install -r requirements.txt
    $ venv/bin/python -m tox -e py38

Installation
------------

You can install mosaik system-wide like so:

::

    $ pip install mosaik-csv
